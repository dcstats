set terminal png size 600, 400
set output "dclistsize.png"
set yrange [0:]
set xrange [200:104857600]
set logscale x
set xlabel "file list size"
set ylabel "number of file lists"
set key box center bottom
set grid xtics ytics
set xtics ("1K" 1024, "10K" 10240, "100K" 102400,\
           "1M" 1048576, "10M" 10485760, "100M" 104857600)
plot "dclistsize"   using (2**(floor(log($1)/log(2)))):(1.0) smooth freq with histeps title "compressed",\
     ""             using (2**(floor(log($2)/log(2)))):(1.0) smooth freq with histeps title "uncompressed"
