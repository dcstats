set terminal png size 600, 400
set output "dcfilesize.png"
#set yrange [0:1.5e7]
set xrange [780:1.1e10]
set logscale x
set xlabel "file size"
set ylabel "number of files (millions)"
set nokey
set grid xtics ytics
set xtics ("1K" 1024, "10K" 10240, "100K" 102400,\
           "1M" 1048576, "10M" 10485760, "100M" 104857600,\
           "1G" 1073741824, "10G" 10737418240)
plot "dcfilesize" using (2**(9+$1) + 2**(8+$1)):($2/1e6) with histeps
