set terminal png size 600, 400
set output "dclistcomp.png"
set xrange [0:100]
set xlabel "compression ratio (compressed/uncompressed*100)"
set ylabel "number of file lists"
set nokey
set grid xtics ytics
plot "dclistsize" using (floor($1/$2*100)):(1.0) smooth freq with histeps
