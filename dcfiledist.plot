set terminal png size 600, 400
set output "dcfiledist.png"
set yrange [0.7:1e7]
set xrange [1:600]
set logscale y
set xlabel "number of users"
set ylabel "number of files"
set nokey
set grid xtics ytics
plot "dcfiledist" with histeps
