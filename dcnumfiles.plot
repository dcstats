set terminal png size 600, 400
set output "dcnumfiles.png"
set yrange [0:]
set xrange [1:1e6]
set logscale x
set xlabel "number of files"
set ylabel "number of file lists"
set key box center bottom
set grid xtics ytics
# Empty file lists screw things up, hence grep
plot "< grep -v ^0 dcnumfiles" using (2**(floor(log($1)/log(2)))):(1.0) smooth freq with histeps title "unique",\
     ""                        using (2**(floor(log($2)/log(2)))):(1.0) smooth freq with histeps title "all"
